import argparse
import os
from decode import MORSE_CODE, MORSE_CONVERTER

MORSE_CODE_REVERSE = {value: key for key, value in MORSE_CODE.items()}
# We need to convert single spaces to 5 spaces between words
del MORSE_CODE_REVERSE[" "]
MORSE_CODE_REVERSE[" "] = "     "

MORSE_CONVERTER_REVERSE = {value: key for key, value in MORSE_CONVERTER.items()}


def main():
    """Parse args and call encode()"""
    parser = argparse.ArgumentParser(description='Encode a message')
    parser.add_argument('message', type=str,
                        help='The message to encode')
    parser.add_argument('-n', metavar='iterations', type=int, default=1,
                        help='The number of layers to hide the message under')
    parser.add_argument('--file', metavar='file', type=str, default="code.txt",
                        help='The name of the output file')

    arguments = parser.parse_args()
    if arguments.n > 4:
        print("WARNING: When n is greater than 4, this program takes a long time to run")
        answer = input("Are you sure you wish to do that? y/N\n")
        if answer != 'y':
            print("Cancelled")
            return
        else:
            # Encode and write out one char at a time to reduce RAM usage
            if os.path.exists(arguments.file):
                os.remove(arguments.file)
            with open(arguments.file, "a") as f:
                for letter in arguments.message:
                    f.write(encode(letter.upper(), arguments.n))
                    f.write(" ")
    else:
        result = encode(arguments.message.upper(), arguments.n)
        with open(arguments.file, "w") as f:
            f.write(result)
    print("Your message has been encoded and written to the file \"%s\"" % f.name)


def convert_to_morse(message):
    """Converts letters to regular morse code"""
    encoded_message = [MORSE_CODE_REVERSE[char] for char in message]
    return " ".join(encoded_message)


def encode(message, iterations):
    """Converts regular morse code to DOTs and DASHes recursively"""
    message = convert_to_morse(message)
    if iterations == 0:
        return message
    encoded_message = []
    for char in message:
        encoded_message.append(MORSE_CONVERTER_REVERSE[char])
    return encode(" ".join(encoded_message), iterations - 1)


if __name__ == '__main__':
    main()
