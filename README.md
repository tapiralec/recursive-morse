# recursive-morse

"You think anybody's ever done the joke where you do the dots and dashes for the
letter D then the letter A then the letter S the the letter H, and then you
spell out DASH and then in the meta of that, you spell out DASH again?"
[- Nick Reineke](https://clips.twitch.tv/BlushingKnottyHerringVoHiYo)

This project recursively translates a message as described
in the above quote.

## Usage

To encode a message with 3 iterations and send the output to a file, `code.txt`
you can run the following command:

```
python3 encode.py "I've been kidnapped" -n 3 --file code.txt
```

 * If n=0, the message will just be converted to normal morse code
 * The default value of `-n` is 1
   * Note that this program gets *exponentially* slower as n increases.
   n=5 can take a whole minute. n=6 freezes my computer.
 * The default value of `--file` is `code.txt` (so it can be omitted)

To decode the message stored in `code.txt`, run the following command:

```
python3 decode.py --file code.txt
```

 * The default value of `--file` is `code.txt` (so it can be omitted)

## Differences from Nick's original description

In morse code, each letter is separated by a space and each word is separated by
7 spaces. These spaces are needed to generate the final message. So in addition
to DOT and DASH, I also used the word SPACE.

## Installation

**Note:** You must have Python 3.5 or greater installed
```
git clone https://gitlab.com/mac-chaffee/recursive-morse.git
cd recursive-morse
```
