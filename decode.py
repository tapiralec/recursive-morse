import argparse


MORSE_CODE = {
    " ": " ",
    ".----.": "'",
    ".-.-.-": ".",
    "..--..": "?",
    "_._.--": "!",
    ".-": "A",
    "-...": "B",
    "-.-.": "C",
    "-..": "D",
    ".": "E",
    "..-.": "F",
    "--.": "G",
    "....": "H",
    "..": "I",
    ".---": "J",
    "-.-": "K",
    ".-..": "L",
    "--": "M",
    "-.": "N",
    "---": "O",
    ".--.": "P",
    "--.-": "Q",
    ".-.": "R",
    "...": "S",
    "-": "T",
    "..-": "U",
    "...-": "V",
    ".--": "W",
    "-..-": "X",
    "-.--": "Y",
    "--..": "Z",
    "-----": "0",
    ".----": "1",
    "..---": "2",
    "...--": "3",
    "....--": "4",
    ".....": "5",
    "-....": "6",
    "--...": "7",
    "---..": "8",
    "----.": "9",
}

MORSE_CONVERTER = {
    "DOT": ".",
    "DASH": "-",
    "SPACE": " ",
}


def main():
    """Decode morse code recursively"""
    parser = argparse.ArgumentParser(description='Encode a message')
    parser.add_argument('--file', metavar='file', type=str, default="code.txt",
                        help='The name of the output file')
    with open(parser.parse_args().file, "r") as f:
        code = "".join([line.strip() for line in f])

    letters = split_morse(code)
    print(decode(letters))


def split_morse(code):
    """Split morse code into a list of morse letters and spaces"""
    # Words are delimited by 7 spaces
    words = code.split("       ")
    letters = []
    for word in words:
        letters.extend(word.split())
        letters.append(" ")
    # Remove the trailing delimiter
    return letters[:-1]


def decode(letters):
    """Recursively decode lists of morse letters and spaces"""
    decoded_text = "".join([MORSE_CODE[letter] for letter in letters])
    if decoded_text.startswith("DOT") or \
       decoded_text.startswith("DASH") or \
       decoded_text.startswith("SPACE"):
        return decode(convert_to_morse(decoded_text))
    return decoded_text


def convert_to_morse(text):
    """Convert the words DOT, DASH, and SPACE into symbols, then split them"""
    return split_morse("".join(MORSE_CONVERTER[word] for word in text.split()))


if __name__ == '__main__':
    main()
